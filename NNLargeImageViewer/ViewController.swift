//
//  ViewController.swift
//  NNLargeImageViewer
//
//  Created by Terry Choy on 2020/3/11.
//  Copyright © 2020 Terry Choy. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIScrollViewDelegate {
    
    @IBOutlet weak var aScrollView: UIScrollView?
    var aImageView: NNLargeImageView?

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.

        if let image = UIImage.init(named: "bigimage.jpg") {
            let size = image.size
            let smallSize = self.aScrollView!.frame.size
            let scale = smallSize.width / size.width
            let aImageView = NNLargeImageView.init(frame: CGRect.init(x: 0, y: 0, width: smallSize.width, height: size.height * scale), image: image)
            self.aImageView = aImageView
            self.aScrollView?.addSubview(aImageView)
            self.aScrollView?.minimumZoomScale = 1
            self.aScrollView?.maximumZoomScale = size.width / smallSize.width

            self.keepCenter(scrollView: self.aScrollView!, contentView: aImageView)
        }
    }
    
    func keepCenter(scrollView: UIScrollView, contentView: UIView) {
        let offset = CGPoint.init(
            x: scrollView.bounds.size.width > scrollView.contentSize.width ?
                (scrollView.bounds.size.width - scrollView.contentSize.width) / 2 : 0,
            y: scrollView.bounds.size.height > scrollView.contentSize.height ?
                (scrollView.bounds.size.height - scrollView.contentSize.height) / 2 : 0)
        contentView.center = CGPoint.init(x: scrollView.contentSize.width / 2 + offset.x,
                                          y: scrollView.contentSize.height / 2 + offset.y)
    }
    
    // MARK: - UIScrollViewDelegate
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.aImageView
    }
    
    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        if let contentView = self.aImageView {
            self.keepCenter(scrollView: scrollView, contentView: contentView)
        }
    }
    
    

}


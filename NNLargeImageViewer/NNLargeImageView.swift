//
//  NNLargeImageView.swift
//  NNLargeImageViewer
//
//  Created by Terry Choy on 2020/3/11.
//  Copyright © 2020 Terry Choy. All rights reserved.
//

import UIKit

class NNLargeImageView: UIView {
    
    var image: UIImage?
    var imageScale: CGFloat?
    
    override class var layerClass: AnyClass {
        get {
            return NNTiledLayer.self
        }
    }
    
    init(frame: CGRect, image: UIImage) {
        super.init(frame: frame)
        self.backgroundColor = .red
        self.image = image;

        let size = self.image!.size
        let smallSize = self.frame.size
        let imageScale = smallSize.width / size.width
        self.imageScale = imageScale
        let tiledLayer: NNTiledLayer = self.layer as! NNTiledLayer
        let level: Int = Int(ceil(log2(1 / imageScale)))
        tiledLayer.levelsOfDetail = 1
        tiledLayer.levelsOfDetailBias = level
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        autoreleasepool {
            let r = CGRect.init(x: rect.origin.x / self.imageScale!, y: rect.origin.y / self.imageScale!, width: rect.size.width / self.imageScale!, height: rect.size.height / self.imageScale!)
            let cropImage = self.image!.cgImage!.cropping(to: r)
            let tileImage = UIImage.init(cgImage: cropImage!)
            tileImage.draw(in: rect)
        }
    }

}

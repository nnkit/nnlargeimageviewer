//
//  NNTiledLayer.swift
//  NNLargeImageViewer
//
//  Created by Terry Choy on 2020/3/11.
//  Copyright © 2020 Terry Choy. All rights reserved.
//

import UIKit

class NNTiledLayer: CATiledLayer {

    override class func fadeDuration() -> CFTimeInterval {
        return 0
    }
}
